"""
Person Sorting

This script defines a list of people with their names, last names, and ages. It then performs various sorting operations on the list and displays the results to the user.

Data:
- personnes (list): A list of people with their information.

Functions:
- custom_sort: A custom sorting function used to sort the list based on names and last names.
- format_2d_array: Formats a 2D array as a string with newline-separated rows.

Sorting Operations:
1. Display a list_1 sorted alphabetically by names.
2. Display a list_2 sorted in reverse alphabetical order by last names.
3. Display a list_3 sorted in ascending order by ages.
4. Display a list_4 sorted alphabetically by names. If names are identical, sort them alphabetically by last names.

Usage:
Run the script to see the results of each sorting operation.

Example:
python script_name.py
"""

import random

def format_2d_array(array_2d):
    """
    Formats a 2D array as a string with newline-separated rows.

    Parameters:
    - array_2d (list): The 2D array to format.

    Returns:
    - str: The formatted string.
    """
    return "\n".join(map(str, array_2d))

def custom_sort(personne):
    """
    Custom sorting function for sorting people by name and last name.

    Parameters:
    - personne (list): A person's information.

    Returns:
    - tuple: A tuple representing the sorting key.
    """
    return (personne[0], personne[1])  # Sort first by name, then by last name

# Data
personnes = [
    ["Pascal", "ROSE", "43 ans"],
    ["Mickaël", "FLEUR", "29 ans"],
    ["Henri", "TULIPE", "35 ans"],
    ["Michel", "FRAMBOISE", "35 ans"],
    ["Arthur", "PETALE", "35 ans"],
    ["Michel", "POLLEN", "50 ans"],
    ["Maurice", "FRAMBOISE", "42 ans"]
]

random.shuffle(personnes)

# Sorting Operations
print("1) List sorted alphabetically by names:")
print(format_2d_array(sorted(personnes, key=lambda x: x[0], reverse=False)))

print("\n2) List sorted in reverse alphabetical order by last names:")
print(format_2d_array(sorted(personnes, key=lambda x: x[1], reverse=True)))

print("\n3) List sorted in ascending order by ages:")
print(format_2d_array(sorted(personnes, key=lambda x: x[2], reverse=False)))

print("\n4) List sorted alphabetically by names. If names are identical, sorted alphabetically by last names:")
personnes_triees = sorted(personnes, key=custom_sort)
print(format_2d_array(personnes_triees))
